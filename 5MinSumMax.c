#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#define N 5

int main()
{
	int mass[N]={0}, max=-32768, min=32767, sum=0;
	short i=0, iMax=0, iMin=0;
	srand(time(0));
	for (i=0;i<N;i++)
	{
		mass[i] = rand() % 20 * pow(-1, rand()%2+1);
		if (mass[i]<min)
		{
			min = mass[i];
			iMin = i;
		}
		if (mass[i]>max)
		{
			max = mass[i];
			iMax = i;
		}
		printf("%d\n", mass[i]);
	}
	if (iMax>iMin)
	{
		for (i=iMin+1;i<iMax;i++)
			sum = sum + mass[i];
	}
	else
	{
		for (i=iMax+1;i<iMin;i++)
			sum = sum + mass[i];
	}
	printf("Max and Min: %d %d\n", max, min);
	printf("Summa %d\n", sum);
	return 0;
}