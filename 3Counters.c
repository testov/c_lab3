#include <stdio.h>
#include <math.h>
#define N 80
#define M 3

int main()
{
	char str[N];
	int valueCurr=0, factor=0, word=0, i=0, k=0, Ind=0;
	int sumBuff[5]={0}, sum=0;
	puts("Enter a string");
	fgets(str, N, stdin);
	while (str[i])
	{
		if (word == 0 && str[i]!=' ')
		{	
			word = 1;
			factor = M;
		}
		else if ((word == 1 && str[i]==' ') || (word == 1 && str[i+1]=='\0'))
		{
			word = 0;
			if (factor!=0)
				sumBuff[k] = sumBuff[k]/(pow(10, factor));
			for (Ind=0;Ind<=k;Ind++)
			{
				sum = sum + sumBuff[Ind];
				sumBuff[Ind] = 0;
			}			
			k = 0;
		}
		if (word == 1)
		{
			valueCurr = str[i] - '0';
			sumBuff[k] = sumBuff[k] + pow(10, factor-1)*valueCurr;
			factor--;
			if (factor==0)
			{
				factor = M;
				k++;
			}
		}
		i++;
	}
	printf("Summa of numbers equals to %d\n", sum);
	return 0;
}