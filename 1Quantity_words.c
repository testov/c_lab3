#include <stdio.h>
#define N 80

int main()
{
	char str[N], i=0;
	short word = 0, count = 0;
	puts("Please enter a string");
	fgets(str, N, stdin);
	while (str[i])
	{
		if (word == 0 && str[i]!=' ')
		{
			count++;
			word = 1;
		}
		else if (word == 1 && str[i]==' ')
		{
			word = 0;
			putchar('\n');
		}

		if (word == 1)
			putchar(str[i]);
		i++;
	}
	return 0;
}