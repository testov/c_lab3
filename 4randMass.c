#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define N 10

int main()
{
	int mass[N] = {0}, i=0, otr=0, sum=0;
	srand(time(0));
	for (i=0;i<N;i++)
	{
		mass[i] = rand() % 20 * pow(-1, rand()%2+1);
		if (otr == 1)
			sum = sum + mass[i];
		if (mass[i]<0 && otr == 0)
			otr = 1;
		printf("%d\n", mass[i]);
	}
	for(i=N-1;i>0;i--)
	{

		if (mass[i]>0 && otr == 1)
		{
			otr = 0;
			break;
		}
		sum = sum - mass[i];
	}
	printf("Summa of elements equals to %d\n", sum);
	return 0;
}