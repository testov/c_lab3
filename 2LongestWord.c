#include <stdio.h>
#include <string.h>
#define N 80

int main()
{
	char str[N], i=0, iCurr = 0, counter=0;
	short word = 0;
	int len = 0;
	puts("Please enter a string");
	fgets(str, N, stdin);
	while(str[i])
	{
		if (word == 0 && str[i]!=' ')
		{
			counter = 0;
			word = 1;
		}
		else if ((word == 1 && str[i]==' ') || (word == 1 && str[i+1]=='\0'))
		{
			word = 0;
			if (counter>len)
			{
				len = counter;
				iCurr = i - len;
			}
		}
		i++;
		counter++;
	}
	for (i=iCurr;i<iCurr+len;i++)
	{
		putchar(str[i]);
	}
	putchar(' ');
	printf("%d \n", len);
	return 0;
}